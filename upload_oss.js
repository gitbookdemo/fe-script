/*
 * @Author: hucheng
 * @Date: 2020-05-10 15:25:18
 * @Description: 演示上传到 阿里云oss
 */
const fs = require("fs");
const path = require("path");
const OSS = require("ali-oss");
const CWD = process.cwd();
const logger = log();
const config = {
  ossConfig: {
    accessKeyId: "",
    accessKeySecret: "",
    region: "",
    bucket: "",
  },
  projectConfig: {
    distDir: "dist",
  },
};

const projectName = process.env.$TEST_PROJECT_NAME; //  TEST_PROJECT_NAME   是在 .gitlab-ci.yml 中 variables 属性 配置的
deploy(projectName);
function deploy(projectName) {
  const projectConfig = config.projectConfig;
  const filePathArray = getFilePathForDir(
    path.resolve(CWD, projectConfig.distDir),
    []
  );
  filePathArray.map((ele) => {
    uploadALiOss(`/${projectName}/${ele.split("/").pop()}`, ele);
  });
}
async function uploadALiOss(key, file) {
  const ossConfig = config.ossConfig;
  const client = new OSS({
    region: ossConfig.region,
    accessKeyId: ossConfig.accessKeyId,
    accessKeySecret: ossConfig.accessKeySecret,
    bucket: ossConfig.bucket,
  });
  const result = await client.put(`fe/${key}`, file);

  logger.info(`upload success!,url is ${result.url}`);
  return result.url;
}
/**
 *
 * @param {String} docPath
 * @param {Array} resultArray
 */
function getFilePathForDir(docPath, resultArray) {
  if (!fs.existsSync(docPath)) {
    throw new Error(`${docPath} is not exit!`);
  }
  const array = fs.readdirSync(docPath);
  array.forEach(function (ele, index) {
    let info = fs.statSync(docPath + "/" + ele);
    if (info.isDirectory()) {
      getFilePathForDir(docPath + "/" + ele, resultArray);
    } else {
      resultArray.push(path.join(docPath, ele));
    }
  });
  return resultArray;
}

function log() {
  const baseMesasge = "gitlab ci: ";
  return {
    error: function (mesasge) {
      console.error(`${baseMesasge}${mesasge}`);
    },
    warn: function (mesasge) {
      console.warn(`${baseMesasge}${mesasge}`);
    },
    info: function (mesasge) {
      console.info(`${baseMesasge}${mesasge}`);
    },
    log: function (mesasge) {
      console.info(`${baseMesasge}${mesasge}`);
    },
  };
}
